const { initTracer } = require("jaeger-client");
const defaultReporter = {
  logSpans: true,
}

module.exports.initTracer = (serviceName, reporter = defaultReporter) => {
  const config = {
    serviceName,
    sampler: {
      type: "const",
      param: 1,
    },
    reporter,
  };
  const options = {
    logger: {
      info(msg) {
        console.log("INFO ", msg);
      },
      error(msg) {
        console.log("ERROR", msg);
      },
    },
  };
  return initTracer(config, options);
};