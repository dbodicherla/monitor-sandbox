const path = require('path');
const createError = require('http-errors');
const responseTime = require('response-time')
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const { initTracer } = require("./lib/tracing");
const jaegerConfig = require('./configs/jaeger');
const tracer = initTracer("app_backend", {
  ...jaegerConfig,
  logSpans: true,
});

const indexRouter = require('./routes/index');
const issuesRouter = require('./routes/issues');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(responseTime(function (req, res, time) {
  const span = tracer.startSpan("responseTime");
  const ctx = { span };
  span.setTag("route", req.url);
  ctx.span.log({
    event: "load_time",
    value: time,
  });
  ctx.span.finish();
}))

app.use('/', indexRouter);
app.use('/issues', issuesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
