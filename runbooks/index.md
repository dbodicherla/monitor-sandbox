## Runbooks

When metrics dashboard shows:
1. Empty message
    1. The Prom
    1. This could be because the configured metrics dashboard does not have any panels configured.
1. Error message
    1. If the error message says 
        1. aaaabbbccccc
            1. This is because aaa 
        1. ddddeeeeffff
            1. This is because dddd
1. Unable to connect to prometheus
    1. Make sure the Prometheus instance is connected.
    1. Make sure either the manual or auto configuration is valid.
1. Empty charts
    1. The configured panel metrics does not return data.
    1. Navigate to `https://log.gprd.gitlab.net/` and check the PromQL query.
