const express = require('express');
const router = express.Router();
const getIssues = require('../services/issues');

/* GET issues listing. */
router.get('/:projectId', async function(req, res, next) {
  const issues = await getIssues(req.params.projectId);
  res.render('issues', { title: 'Issues', issues });
});

module.exports = router;
