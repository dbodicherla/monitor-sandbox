const express = require('express');
const router = express.Router();
const getProjects = require('../services/projects');
const jaegerUrl = process.env.JAEGER_URL;

/* GET home page. */
router.get('/', async function(req, res, next) {
  const projects = await getProjects();
  res.render('index', { title: 'Monitor Sandbox Project', projects, jaegerUrl });
});

module.exports = router;
