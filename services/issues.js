const axios = require('axios');
const gitlabConfig = require('../configs/gitlab');
const URL = 'https://gitlab.com/api/v4';
const params = {
  headers: {
    'PRIVATE-TOKEN': gitlabConfig.token,
  }
};
const COLUMNS = ['title', 'description', 'state'];

const pluckIssuesColumns = issue => 
  COLUMNS.reduce((acc, curr) => {
    acc[curr] = issue[curr]
    return acc;
  }, {});

const parseIssuesResponse = response => response.data.map(pluckIssuesColumns);


const getIssues = projectId =>
  new Promise((resolve, reject) => {
    axios.get(`${URL}/projects/${projectId}/issues`, params)
      .then(function (response) {
        // handle success
        resolve(parseIssuesResponse(response));
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  })

module.exports = getIssues;