const axios = require('axios');
const gitlabConfig = require('../configs/gitlab');
const URL = 'https://gitlab.com/api/v4';
const params = {
  headers: {
    'PRIVATE-TOKEN': gitlabConfig.token,
  }
};
const COLUMNS = ['id', 'description', 'name', 'web_url', 'star_count', 'forks_count'];

const pluckProjectColumns = project => 
  COLUMNS.reduce((acc, curr) => {
    acc[curr] = project[curr]
    return acc;
  }, {});

const parseProjectsResponse = response => response.data.map(pluckProjectColumns);


const getProjects = () =>
  new Promise((resolve, reject) => {
    axios.get(`${URL}/groups/9970/projects?order_by=updated_at`, params)
      .then(function (response) {
        // handle success
        resolve(parseProjectsResponse(response));
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  })

module.exports = getProjects;