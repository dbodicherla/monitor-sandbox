const jaegerConfig = {
  collectorEndpoint: process.env.JAEGER_COLLECTOR_ENDPOINT || 'http://jaeger-collector.default.svc.cluster.local:14268/api/traces',
};

module.exports = jaegerConfig;