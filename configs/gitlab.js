const gitlabConfig = {
  token: process.env.GITLAB_API_TOKEN,
};
  
module.exports = gitlabConfig;
